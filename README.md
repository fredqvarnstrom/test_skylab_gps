# Testing Skylab SKM56J GPS module with Raspberry Pi 3
========================================================


## Components

- Skylab SKM56J GPS module
    
    http://www.skylab.com.cn/en/productview-84.html

- Raspberry Pi 3
    
    https://www.raspberrypi.org/products/raspberry-pi-3-model-b/
    
    
## Preparing
    
- Expand file system on RPi.
    
    https://www.raspberrypi.org/documentation/configuration/raspi-config.md

- Disable Serial login on RPi.
    
    http://raspberrypi.stackexchange.com/questions/45570/how-do-i-make-serial-work-on-the-raspberry-pi3
    
    
## Connect GPS module with RPi 3
    
| **GPS**  | **RPi**   |
| ----     |:-------:  |
| GND      | GND       |
| VIN      | 3.3V      |
| TxD      | GPIO 15   |
| RxD      | GPIO 14   |

## Test GPS module
    
- Install dependencies for GPS data.
    
        sudo apt-get install python-pip
        sudo pip install pyserial
        
- Acquire GPS data from the module.
    
    Use the command

        stty -F /dev/ttyS0

    to see the current settings.

    and

        stty -F /dev/ttyS0 9600

    to set a baud rate of 9600 (4800 is more normal for GPS).

    Try

        cat </dev/ttyS0

    to see if your GPS unit is properly connected.

    ![GPS data](gps_test.jpg "GPS Test")
    
    Or something like this:
        
        $GPGSA,A,1,,,,,,,,,,,,,,,*1E
        $GPGSV,2,1,06,18,67,304,42,20,46,103,33,21,62,214,34,24,49,135,00*7C
        $GPGSV,2,2,06,28,03,033,00,32,,,27,,,,,,,,*42
        $GPRMC,195854,V,4425.8867,N,07543.5346,W,000.0,000.0,151116,,,N*7B
        $GPGGA,195855,4425.8867,N,07543.5346,W,0,00,,00000.0,M,-034.0,M,,*66
        $GPGSA,A,1,,,,,,,,,,,,,,,*1E
        $GPGSV,2,1,05,18,67,304,42,20,46,103,34,21,62,214,35,24,49,135,00*79
        $GPGSV,2,2,05,28,03,033,00,,,,,,,,,,,,*45
        $GPRMC,195855,V,4425.8867,N,07543.5346,W,000.0,000.0,151116,,,N*7A
    
- Detailed explaination
    
        $GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
        1    = UTC of Position
        2    = Latitude
        3    = N or S
        4    = Longitude
        5    = E or W
        6    = GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)
        7    = Number of satellites in use [not those in view]
        8    = Horizontal dilution of position
        9    = Antenna altitude above/below mean sea level (geoid)
        10   = Meters  (Antenna height unit)
        11   = Geoidal separation (Diff. between WGS-84 earth ellipsoid and
               mean sea level.  -=geoid is below WGS-84 ellipsoid)
        12   = Meters  (Units of geoidal separation)
        13   = Age in seconds since last update from diff. reference station
        14   = Diff. reference station ID#
        15   = Checksum

    
## Get correct location from GPS.
    
Since we are getting GPS data from serial bus, we can use any type of programming language to parse them.

(The data is in format of **NMEA** standard.)

Let's use Python then.
    
- Install package
    
        sudo pip install pyserial
        
- Test code:
    
        #!/usr/bin/env python
        
        import serial
        
        ser = serial.Serial(port='/dev/ttyS0', baudrate=9600, timeout=.5)
        
        for line in ser.readline().split('\n') :
            if line.startswith('$GPGGA') :
                lat, _, lon = line.strip().split(',')[2:5]
                print "Latitude: {}, Longitude: {}".format(lat, lon)
                